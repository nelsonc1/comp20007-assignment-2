/*
 * NAME: NELSON KAI MIIN CHEN
 * STUDENT NO.: 743322
*/
#include "hash.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXSTRLEN 256

/* Used as the second hashing function on double hash */
unsigned int linear_probe(void *e, unsigned int size) {

    //return constant increment for linear growth
    return 1;
}

/* Very simple hash */
unsigned int worst_hash(void *e, unsigned int size) {
    (void) e;
    (void) size;
    return 0;
}

/* Basic numeric hash function */
unsigned int num_hash(long n, unsigned int size) {
    return n % size;
}

/* Bad hash function */
unsigned int bad_hash(char *key, unsigned int size) {
    static int a, rnum = 0;

    // Check for randomised number
    if (rnum == 0){
        a = rand() % size;
        rnum = 1;
    }

    return (a * key[0]) % size;
}

/* Universal hash function as described in Dasgupta et al 1.5.2 */
unsigned int universal_hash(unsigned char *key, unsigned int size) {
    unsigned int i, hash_num=0;
    static unsigned int r[MAXSTRLEN], rnum=0;
    unsigned char *token;

    // Check for randomised array of numbers
    if (rnum==0){
      for (i=0; i<MAXSTRLEN; ++i){
        r[i] = rand() % size;
      }
      rnum = 1;
    }

    // Remove newline character
    token = strtok(key, "\n");

    // Calculate hash value within range {0,1,...,size-1}
    for (i=0; i<strlen(token); i++){
        hash_num += (r[i]*(int)token[i]) % size;
    }

    return hash_num % size;
}
