/*
 * NAME: NELSON KAI MIIN CHEN
 * STUDENT NO.: 743322
*/
#include "extra.h"
#include "hash.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <ctype.h>
#include <string.h>

#define MAXSTRLEN 256

typedef struct {
    int x;
    int y;
    int d;
} Tuple;

int check_prime(int n);
Tuple extended_euclid(int a, int b);

/* Determine appropriate size of a hash table given input size n */
unsigned int determine_size(unsigned int n) {
    int size = 2*n;

    // Search for prime size
    while (check_prime(size) != 1){
        size++;
    }

    return size;
}

/* Check input n is a prime number */
int check_prime(int n){
    int isprime, divisor;

    isprime = 1;
    for (divisor=2; divisor*divisor<=n; divisor++) {
        if (n%divisor==0) {
          isprime = 0;
          break;
        }
    }

    return isprime;
}

/* Print n strings that are hashed to 0 by universal_hash seeded with seed */
void collide_dumb(unsigned int size, unsigned int seed, int n) {
    // Brute force to find n value that hashes to 0 by universal_hash
    int count;
    char c;
    unsigned char *key;
    static unsigned int r[MAXSTRLEN], i, key_i, rnum=0;

    srand(seed);

    // Generate a static list of random values
    if (rnum==0){
      printf("NUM_RANDOMS: %d\n", MAXSTRLEN);

      for (i=0; i<MAXSTRLEN; ++i){
          r[i] = rand() % size;
          while (r[i] < 1){
              r[i] = rand() % size;
          }
          printf("r[%d]: %d\n", i, r[i]);
      }
      printf("\n");
      rnum = 1;
    }

    count = 0;
    // Generate n strings
    while (count != n){

        // Allocate string memory space
        key = malloc((strlen(key)+1)*size);
        assert(key);

        // Generate random alphanumeric strings
        key_i = 0;
        while (key_i < size){
            c = rand() + '0';
            // Check for alphanumeric value
            if (isalnum(c)){
                key[key_i] = c;
                key_i++;
            }
        }

        // Check random string to be hashed to 0
        if (universal_hash(key,size) == 0){
            for (i=0; i<size; i++){
                printf("%c", key[i]);
            }
            printf("\n");
            count++;
        }
        // Clear string memory
        free(key);
    }
    printf("\n");
}

/* Print n strings that are hashed to 0 by universal_hash seeded with seed */
void collide_clever(unsigned int size, unsigned int seed, int n) {
    int i, count=0, value, v, k=1;
    Tuple eu;
    unsigned char *key;
    static unsigned int r[MAXSTRLEN], s[MAXSTRLEN], rnum = 0;

    srand(seed);

    // Generate a static list of random values
    if (rnum==0){
        printf("NUM_RANDOMS: %d\n", MAXSTRLEN);

        for (i=0; i<MAXSTRLEN; ++i){
            r[i] = rand() % size;
            while (r[i] < 1){
                r[i] = rand() % size;
            }
            printf("r[%d]: %d\n", i, r[i]);
        }
        printf("\n");
        rnum = 1;
    }

    // Generat n number of strings
    while (count != n){
        key = malloc(sizeof(key)*n);
        assert(key);
        for (i=0; i<n; i++){
            // Use extended Euclid to find x, y and d
            eu = extended_euclid(r[i], (int)size);

            // Normalise x to be within range of size
            if (eu.x < 1){
                s[i] = (eu.x % (int)size) + (int)size;
            } else {
                s[i] = eu.x % (int)size;
            }

            // Calculate V
            value = (r[i]*s[i]) % size;
            if (i==n-1){
                v = size - (n) + 1;
                key[i] = v*k;
            } else {
                key[i] = value*k;
            }
        }

        // End of string
        key[i++] = NULL;
        printf("%s\n", key);
        k++;
        free(key);
        count++;
    }
}

/* This Algorithm is taken from Algorithms (DPV, 2006) Figure 1.6 */
Tuple extended_euclid(int a, int b){

    Tuple tuple;
    Tuple prime;

    if (b==0){
        tuple.x = 1;
        tuple.y = 0;
        tuple.d = a;
        return tuple;
    }

    prime = extended_euclid(b, a % b);

    tuple.d = prime.d;
    tuple.x = prime.y;
    tuple.y = prime.x - (a/b)*prime.y;

    return tuple;
}
